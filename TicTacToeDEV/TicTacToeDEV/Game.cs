﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TicTacToeDEV.Commands;
using TicTacToeDEV.GameTypes;
using System.Threading;

namespace TicTacToeDEV
{
    internal class Game : IGame, ISetterGame, IPrinterGame
    {
        #region Constants
        public const string ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
        #endregion

        #region Attributes
        // Flags of game.
        private bool _isExitOptionEnabled;
        private bool _isConsecutiveModeEnabled;
        private bool _isSilentModeEnabled;
        private Board _board;
        private bool _isGameStarted;
        private Player _playerA;
        private Player _playerB;
        private int _playedGames;
        private int _maxLengthOfBoard;
        private GameAnalyzer _gameAnalyzer;
        private Point _lastMovePlayerPosition;
        private BoardPrinter _boardPrinter;
        private Player _lastPlayerWhoPlayed;
        #endregion

        #region Properties
        public bool IsGameStarted
        {
            get { return _isGameStarted; }
        }

        public int MaxLengthOfBoard
        {
            get { return _maxLengthOfBoard; }
        }
        #endregion

        #region Constructors
        public Game()
        {
            // Setting console.
            Console.Title = "TicTacToe";

            // Setting the game.
            _isExitOptionEnabled = false;
            _isGameStarted = false;
            _playedGames = 0;
            _playerA = new Player(Mark.O);
            _playerB = new Player(Mark.X);
        }
        #endregion

        #region Methods
        public void ExecuteGameLoop()
        {
            Console.WriteLine("Hello! How about a game of TicTacToe?");
            while (_isExitOptionEnabled == false)
            {
                string[] commandInput = GameInput.CommandReader();
                try
                {
                    var createdCommand = CommandFactory.CreateCommand(this, commandInput);
                    createdCommand.Execute();
                }
                catch (ArgumentException exception)
                {
                    Console.WriteLine(exception.Message);
                }
            }
        }

        public void SettingGame(bool isConsecutiveModeEnabled, bool isSilentModeEnabled)
        {
            _isConsecutiveModeEnabled = isConsecutiveModeEnabled;
            _isSilentModeEnabled = isSilentModeEnabled;
            _isGameStarted = true;
            _gameAnalyzer = new GameAnalyzer(_board);
        }

        public void ExitGame()
        {
            int millisecondsForTimer = 5000;
            Console.WriteLine("Bye!");
            Thread.Sleep(millisecondsForTimer);
            _isExitOptionEnabled = true;
        }

        public void CreateBoard(int sizeOfBoard)
        {
            _board = new Board(sizeOfBoard);
            _maxLengthOfBoard = sizeOfBoard;
        }

        public void PrintScoreData()
        {
            int scorePlayerA = _playerA.EarnedPoints;
            int scorePlayerB = _playerB.EarnedPoints;
            int tieGames = _playedGames - (scorePlayerA + scorePlayerB);
                string scoreDataMessage = "Current Score:\n\t{0} Games played\n\t{1} Games won by {2}\n\t{3} Games won by {4}\n\t{5} Game tied";
            Console.WriteLine(string.Format(scoreDataMessage, _playedGames.ToString(), scorePlayerA.ToString(), _playerA.TeamPlayer.ToString(),
                scorePlayerB.ToString(), _playerB.TeamPlayer.ToString(), tieGames.ToString()));
        }

        public void PrintBoardTable()
        {
            _boardPrinter = new BoardPrinter(_board, _lastMovePlayerPosition);
            _boardPrinter.PrintBoard();
        }

        public void PlayGame(Mark mark, Point position)
        {
            Player currentPlayer = FindPlayerByMark(mark);
            if (!_isConsecutiveModeEnabled && _lastPlayerWhoPlayed == currentPlayer)
            {
                throw new ArgumentException("Error: It is the turn of the next player.");
            }
            _board.SetCellMark(position, currentPlayer);
            _lastMovePlayerPosition = position;
            _lastPlayerWhoPlayed = currentPlayer;
            if (!_isSilentModeEnabled)
            {
                PrintBoardTable();
            }
        }

        public void AnalyzeGame(Mark mark)
        {
            Player currentPlayer = FindPlayerByMark(mark);
            if (_gameAnalyzer.IsAWinner(currentPlayer))
            {
                currentPlayer.AddWinPoint();
                _playedGames++;
                Console.WriteLine(string.Format("Player {0} won", currentPlayer.TeamPlayer.ToString()));
                PrintScoreData();
                ResetGame();
            }
            else if (_gameAnalyzer.IsATie())
            {
                _playedGames++;
                Console.WriteLine("Is a Tie.");
                PrintScoreData();
                ResetGame();
            }
        }

        private void ResetGame()
        {
            _isConsecutiveModeEnabled = false;
            _isSilentModeEnabled = false;
            _isGameStarted = false;
        }

        private Player FindPlayerByMark(Mark mark)
        {
            switch (mark)
            {
                case Mark.O:
                    {
                        return _playerA;
                    }
                default:
                    {
                        return _playerB;
                    }
            }
        } 
        #endregion
    }
}
