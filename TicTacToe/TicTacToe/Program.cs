﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Commands.CommandsConfigurationInfo;

namespace TicTacToe
{
    internal class Program
    {
        static void Main()
        {
            Game game = new Game();
            game.ExecuteGameLoop();
        }
    }
}
