﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Commands.CommandsParametersParser
{
    internal class CommandStartParser
    {
        #region Constants
        private const int NUMBER_OF_MINIMUM_OF_PARAMETERS = 1;
        private const int MINIMUM_BOARD_SIZE_ALLOWED = 1;
        private const int MAXIMUM_BOARD_SIZE_ALLOWED = 9;
        #endregion

        #region Attributes
        private List<string> _parameters;
        private string _username;
        private int _numberOfParametersInList;
        private int _sizeOfBoard;
        private bool _hasConsecutiveParameter;
        private bool _hasSilentParameter;
        private bool _hasHelpParameter;
        private bool _hasOnlineParameter;
        private bool _hasUsernameParameter;
        private bool _hasSizeOfBoard;
        #endregion

        #region Properties
        public bool HasConsecutiveParameter
        {
            get { return _hasConsecutiveParameter; }
        }

        public bool HasSilentParameter
        {
            get { return _hasSilentParameter; }
        }

        public bool HasOnlineParameter
        {
            get { return _hasOnlineParameter; }
        }

        public bool HasHelpParameter
        {
            get { return _hasHelpParameter; }
        }

        public int SizeOfBoard
        {
            get { return _sizeOfBoard; }
        }

        public string Username
        {
            get { return _username; }
        }
        #endregion

        #region Constructor
        public CommandStartParser(List<string> parameters)
        {
            _parameters = parameters;
            _numberOfParametersInList = _parameters.Count;
        }
        #endregion

        #region Methods
        public bool HasCorrectParameters()
        {
            if (IsHelpParameterInParameters())
            {
                return true;
            }
            else
            {
                return HasMinimumOfParameters();
            }
        }

        private bool IsHelpParameterInParameters()
        {
            _hasHelpParameter = _parameters.Exists(x => x == "-H");
            return _hasHelpParameter;
        }

        private bool HasMinimumOfParameters()
        {
            bool hasMinimumOfParameters = _numberOfParametersInList >= NUMBER_OF_MINIMUM_OF_PARAMETERS;
            if (hasMinimumOfParameters)
            {
                ExecuteFindParametersMethods();
                bool isAllParametersWereFound = _parameters.Count == 0;
                return isAllParametersWereFound && HasCorrectCombinationOfParameters();
            }
            else
            {
                return false;
            }
        }

        private void ExecuteFindParametersMethods()
        {
            FindSizeOfBoardParameter();
            FindConsecutiveParameter();
            FindSilentParameter();
            FindOnlineParameter();
        }

        private bool HasCorrectCombinationOfParameters()
        {
            bool isOnlineModeCombination = (_hasOnlineParameter && _hasUsernameParameter && !_hasConsecutiveParameter);
            bool isLocalModeCombination = (!_hasOnlineParameter && !_hasUsernameParameter);
            bool isCorrectCombination = (isOnlineModeCombination ^ isLocalModeCombination) && _hasSizeOfBoard;
            return isCorrectCombination;
        }

        // FindSizeOfBoardParameter: if the parameter is null o not is a numeric parameter, set _sizeOfBoard to 0.
        private void FindSizeOfBoardParameter()
        {
            int sizeOfBoard;

            // Find the first parameter, because that is the size of _board parameter.
            string sizeOfBoardParameter = _parameters.Last();
            if (!string.IsNullOrEmpty(sizeOfBoardParameter))
            {
                bool isNumeric = int.TryParse(sizeOfBoardParameter, out sizeOfBoard);
                if (isNumeric)
                {
                    _hasSizeOfBoard = true;
                    _sizeOfBoard = sizeOfBoard;
                    IsSizeOfBoardInAValidRange(_sizeOfBoard);
                    _parameters.Remove(sizeOfBoardParameter);
                }
            }
        }

        private static bool IsSizeOfBoardInAValidRange(int sizeOfBoard)
        {
            bool isSizeOfBoardInAValidRange = MINIMUM_BOARD_SIZE_ALLOWED <= sizeOfBoard && sizeOfBoard <= MAXIMUM_BOARD_SIZE_ALLOWED;
            if (isSizeOfBoardInAValidRange)
            {
                return true;
            }
            else
            {
                throw new ArgumentException(string.Format("Error: Board size should be between {0} and {1}.", MINIMUM_BOARD_SIZE_ALLOWED, MAXIMUM_BOARD_SIZE_ALLOWED));
            }
        }

        private void FindConsecutiveParameter()
        {
            string parameter = "--CONSECUTIVE";
            _hasConsecutiveParameter = _parameters.Exists(x => x == parameter);
            if (_hasConsecutiveParameter)
            {
                int indexOfParameter = _parameters.FindIndex(x => x == parameter);
                _parameters.RemoveAt(indexOfParameter);
            }
        }

        private void FindSilentParameter()
        {
            string parameter = "--SILENT";
            _hasSilentParameter = _parameters.Exists(x => x == parameter);
            if (_hasSilentParameter)
            {
                int indexOfParameter = _parameters.FindIndex(x => x == parameter);
                _parameters.RemoveAt(indexOfParameter);
            }
        }

        private void FindOnlineParameter()
        {
            string parameter = "--ONLINE";
            _hasOnlineParameter = _parameters.Exists(x => x == parameter);
            if (_hasOnlineParameter)
            {
                int indexOfParameter = _parameters.FindIndex(x => x == parameter);
                _parameters.RemoveAt(indexOfParameter);
                FindUsernameParameter(indexOfParameter);
            }
        }

        private void FindUsernameParameter(int indexOfParameter)
        {
            _hasUsernameParameter = _parameters.Any();
            if (_hasUsernameParameter)
            {
                _username = _parameters[indexOfParameter];
                _parameters.RemoveAt(indexOfParameter);
            }
        }
        #endregion
    }
}
