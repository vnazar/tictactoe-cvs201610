﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Commands.CommandsParametersParser
{
    internal class CommandOnlyHelpParameterParser
    {
        #region Attributes
        private List<string> _parameters;
        private int _numberOfParametersInList;
        private bool _hasHelpParameter;
        #endregion

        #region Properties
        public bool HasHelpParameter
        {
            get { return _hasHelpParameter; }
        }
        #endregion

        #region Constructor
        public CommandOnlyHelpParameterParser(List<string> parameters)
        {
            _parameters = parameters;
            _numberOfParametersInList = _parameters.Count;
        }
        #endregion

        #region Methods
        public bool HasCorrectParameters()
        {
            if (IsHelpParameterInParameters())
            {
                return true;
            }
            else
            {
                bool isNumberOfParametersEqualToZero = _numberOfParametersInList == 0;
                return isNumberOfParametersEqualToZero;
            }
        }

        private bool IsHelpParameterInParameters()
        {
            _hasHelpParameter = _parameters.Exists(x => x == "-H");
            return _hasHelpParameter;
        }
        #endregion
    }
}