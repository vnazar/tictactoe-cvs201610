﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TicTacToeDEV
{
    internal interface ISetterGame
    {
        #region Properties
        bool IsGameStarted { get; }
        int MaxLengthOfBoard { get; }
        #endregion

        #region Methods
        void SettingGame(bool isConsecutiveModeEnabled, bool isSilentModeEnabled);
        void ExitGame();
        void CreateBoard(int sizeOfBoard);
        #endregion
    }
}
