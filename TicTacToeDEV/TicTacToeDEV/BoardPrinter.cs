﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToeDEV.GameTypes;


namespace TicTacToeDEV
{
    internal class BoardPrinter
    {
        #region Attributes
        private Board _board;
        private Point _lastMovePlayerPosition;
        #endregion

        #region Constructors
        public BoardPrinter(Board board, Point lastMovePlayerPosition)
        {
            _board = board;
            _lastMovePlayerPosition = lastMovePlayerPosition;
        }
        #endregion

        #region Methods
        public void PrintBoard()
        {
            VerifyIfBoardIsEmpty();
            PrintRowsHeaders();
            for (int row = 0; row < _board.Length; row++)
            {
                PrintRowElement(row);
            }
        }

        private void VerifyIfBoardIsEmpty()
        {
            if (_board == null)
            {
                throw new ArgumentException("Error: You must start the game before.");
            }
        }

        private void PrintRowsHeaders()
        {
            string rowsHeader = "\t";
            for (int column = 1; column <= _board.Length; column++)
            {
                rowsHeader += string.Format("{0}\t", column.ToString());
            }
            Console.WriteLine(rowsHeader);
        }

        private void PrintRowElement(int row)
        {
            string alphabet = Game.ALPHABET;
            string rowElement = string.Format("{0}\t", alphabet[row].ToString());
            for (int column = 0; column < _board.Length; column++)
            {
                string StringRepresentativeSymbols = ConstructElementString(row, column);
                rowElement += string.Format("{0}\t", StringRepresentativeSymbols);
            }
            Console.WriteLine(rowElement);
        }

        private string ConstructElementString(int row, int column)
        {
            Mark symbol = _board.GetCellMark(new Point(column, row));
            string StringRepresentativeSymbols = GetStringMarkByMarkType(symbol);
            if (_lastMovePlayerPosition != null)
            {
                StringRepresentativeSymbols = AddBracketToTheLastPlayerMark(StringRepresentativeSymbols, new Point(column, row));
            }
            return StringRepresentativeSymbols;
        }

        private static string GetStringMarkByMarkType(Mark mark)
        {
            switch (mark)
            {
                case Mark.O:
                    {
                        return "O";
                    }
                case Mark.X:
                    {
                        return "X";
                    }
                default:
                    return " ";
            }
        }

        private string AddBracketToTheLastPlayerMark(string StringRepresentativeSymbols, Point actualPosition)
        {
            if (actualPosition.PositionX == _lastMovePlayerPosition.PositionX && actualPosition.PositionY == _lastMovePlayerPosition.PositionY)
            {
                return string.Format("[{0}]", StringRepresentativeSymbols);
            }
            return StringRepresentativeSymbols;
        }
        #endregion
    }
}
