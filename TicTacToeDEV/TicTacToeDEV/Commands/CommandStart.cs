﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToeDEV.Commands.CommandsParametersParser;
using TicTacToeDEV.Commands.CommandsConfigurationInfo;

namespace TicTacToeDEV.Commands
{
    internal class CommandStart : ICommandInput
    {
        #region Attributes
        private ISetterGame _setterGame;
        private bool _isConsecutiveModeEnabled;
        private bool _isSilentModeEnabled;
        private bool _isHelpModeEnabled;
        private int _sizeOfBoard;
        #endregion

        #region Constructor
        public CommandStart(ISetterGame setterGame, int sizeOfBoard, bool hasConsecutiveParameter, bool hasSilentParameter, bool hasHelpParameter)
        {
            // Setting command.
            _setterGame = setterGame;
            _isConsecutiveModeEnabled = hasConsecutiveParameter;
            _isSilentModeEnabled = hasSilentParameter;
            _isHelpModeEnabled = hasHelpParameter;
            _sizeOfBoard = sizeOfBoard;
        }
        #endregion

        #region Methods
        public static CommandStart Create(ISetterGame setterGame, List<string> parameters)
        {
            CommandStartParser commandStartAnalyzer = new CommandStartParser(parameters);
            if (commandStartAnalyzer.HasCorrectParameters())
            {
                return new CommandStart(setterGame, commandStartAnalyzer.SizeOfBoard, commandStartAnalyzer.HasConsecutiveParameter, commandStartAnalyzer.HasSilentParameter, commandStartAnalyzer.HasHelpParameter);
            }
            else
            {
                throw new ArgumentException("Error: Unrecognized command parameter.");
            }
        }

        public void PrintHelpMessage()
        {
            string helpMessage = CommandConfigurationReader.GetHelpMessageFromConfigurationInfo("commandStart");
            Console.WriteLine(helpMessage);
        }

        public void Execute()
        {
            if (_isHelpModeEnabled)
            {
                PrintHelpMessage();
            }
            else
            {
                VerifyIfTheGameHasStarted();
                _setterGame.CreateBoard(_sizeOfBoard);
                _setterGame.SettingGame(_isConsecutiveModeEnabled, _isSilentModeEnabled);
            }
        }

        public void VerifyIfTheGameHasStarted()
        {
            if (_setterGame.IsGameStarted)
            {
                throw new ArgumentException("Error: You're playing a game.");
            }
        }
        #endregion
    }
}
