﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToeDEV.Commands.CommandsParametersParser;
using TicTacToeDEV.Commands.CommandsConfigurationInfo;

namespace TicTacToeDEV.Commands
{
    internal class CommandBoard : ICommandInput
    {
        #region Attributes
        private IPrinterGame _printerGame;
        private bool _isHelpModeEnabled;
        #endregion

        #region Constructor

        public CommandBoard(IPrinterGame printerGame, bool hasHelpParameter)
        {
            // Setting command.
            _printerGame = printerGame;
            _isHelpModeEnabled = hasHelpParameter;
        }
        #endregion

        #region Methods
        public static CommandBoard Create(IPrinterGame printerGame, List<string> parameters)
        {
            CommandOnlyHelpParameterParser commandBoardParser = new CommandOnlyHelpParameterParser(parameters);
            if (commandBoardParser.HasCorrectParameters())
            {
                return new CommandBoard(printerGame, commandBoardParser.HasHelpParameter);
            }
            else
            {
                throw new ArgumentException("Error: Unrecognized command parameter.");
            }
        }

        public void PrintHelpMessage()
        {
            string helpMeassage = CommandConfigurationReader.GetHelpMessageFromConfigurationInfo("commandBoard");
            Console.WriteLine(helpMeassage);
        }

        public void Execute()
        {
            if (_isHelpModeEnabled)
            {
                PrintHelpMessage();
            }
            else
            {
                _printerGame.PrintBoardTable();
            }
        }
        #endregion

    }
}
