﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Commands.CommandsParametersParser;
using TicTacToe.Commands.CommandsConfigurationInfo;

namespace TicTacToe.Commands
{
    internal class CommandStart : ICommandInput
    {
        #region Attributes
        private ISetterGame _setterGame;
        private bool _isConsecutiveModeEnabled;
        private bool _isSilentModeEnabled;
        private bool _isOnlineModeEnabled;
        private bool _isHelpModeEnabled;
        private int _sizeOfBoard;
        private string _username;
        #endregion

        #region Constructor
        public CommandStart(ISetterGame setterGame, CommandStartParser commandStartParser)
        {
            // Setting command.
            _setterGame = setterGame;
            _isConsecutiveModeEnabled = commandStartParser.HasConsecutiveParameter;
            _isSilentModeEnabled = commandStartParser.HasSilentParameter;
            _isOnlineModeEnabled = commandStartParser.HasOnlineParameter;
            if (_isOnlineModeEnabled)
            {
                _username = commandStartParser.Username;
            }
            _isHelpModeEnabled = commandStartParser.HasHelpParameter;
            _sizeOfBoard = commandStartParser.SizeOfBoard;
        }
        #endregion

        #region Methods
        public static CommandStart Create(ISetterGame setterGame, List<string> parameters)
        {
            CommandStartParser commandStartParser = new CommandStartParser(parameters);
            if (commandStartParser.HasCorrectParameters())
            {
                return new CommandStart(setterGame, commandStartParser);
            }
            else
            {
                throw new ArgumentException("Error: Unrecognized command parameter.");
            }
        }

        public void PrintHelpMessage()
        {
            string helpMessage = CommandConfigurationReader.GetHelpMessageFromConfigurationInfo("commandStart");
            Console.WriteLine(helpMessage);
        }

        public void Execute()
        {
            if (_isHelpModeEnabled)
            {
                PrintHelpMessage();
            }
            else
            {
                VerifyIfTheGameHasStarted();
                SetupGame();
            }
        }

        public void SetupGame()
        {
            _setterGame.CreateBoard(_sizeOfBoard);
            _setterGame.SetupGame(_isConsecutiveModeEnabled, _isSilentModeEnabled, _isOnlineModeEnabled);
            if (_isOnlineModeEnabled)
            {
                SetupOnlineGame();
            }
            else
            {
                _setterGame.SetupPlayers();
            }
        }

        public void SetupOnlineGame()
        {
            _setterGame.StartOnlineGame();
            _setterGame.SubscribeEventOpponentFound();
            _setterGame.WaitForAOpponent();
            if (_setterGame.OpponentFound)
            {
                _setterGame.SetupPlayers();
            }
        }

        public void VerifyIfTheGameHasStarted()
        {
            if (_setterGame.IsGameStarted)
            {
                throw new ArgumentException("Error: You're playing a game.");
            }
        }
        #endregion
    }
}