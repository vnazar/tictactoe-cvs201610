﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeDEV
{
    internal class Point
    {
        #region Attributes
        private int _positionX;
        private int _positionY;
        #endregion

        #region Properties
        public int PositionX
        {
            get { return _positionX; }
        }

        public int PositionY
        {
            get { return _positionY; }
        }
        #endregion

        #region Constructor
        public Point(int positionX, int positionY)
        {
            _positionX = positionX;
            _positionY = positionY;
        } 
        #endregion
    }
}
