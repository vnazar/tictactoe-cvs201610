﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.GameTypes
{
    enum Command { Start, Play, Board, Score, Help, Exit, Undefined };
}
