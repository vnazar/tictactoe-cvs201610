﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    internal class ExternalComponentAdapter : IExternalComponent
    {
        public event Action<string, bool> OpponentFound;
        public event Action<string> OpponentMoved;

        public void InitializeConnection()
        {
            ExternalComponent.ExternalComponent.InitializeConnection();
        }

        public void RequestOnlineOpponent(int sizeOfBoard)
        {
            ExternalComponent.ExternalComponent.RequestOnlineOpponent(sizeOfBoard);
        }

        public string GetOpponentName()
        {
            return ExternalComponent.ExternalComponent.GetOpponentName();
        }

        public bool LocalPlayerMovesFirst()
        {
            return ExternalComponent.ExternalComponent.LocalPlayerMovesFirst();
        }

        public string GetOpponentLastMove()
        {
            return ExternalComponent.ExternalComponent.GetOpponentLastMove();
        }

        public void SetMyLastMove(string position)
        {
            ExternalComponent.ExternalComponent.SetMyLastMove(position);
        }

        public void CloseConnection()
        {
            ExternalComponent.ExternalComponent.CloseConnection();
        }
    }
}
