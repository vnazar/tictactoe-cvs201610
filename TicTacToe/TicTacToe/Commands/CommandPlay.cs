﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Commands.CommandsConfigurationInfo;
using TicTacToe.Commands.CommandsParametersParser;
using TicTacToe.GameTypes;

namespace TicTacToe.Commands
{
    internal class CommandPlay : ICommandInput
    {
        #region Attributes
        private IGame _gamePlayer;
        private ISetterGame _setterGame;
        private bool _isHelpModeEnabled;
        private int _rowPosition;
        private int _columnPosition;
        private Mark _mark;
        private string _positionString;
        #endregion

        #region Constructor
        public CommandPlay(IGame gamePlayer, ISetterGame setterGame, CommandPlayParser commandPlayParser)
        {
            // Setting command.
            _gamePlayer = gamePlayer;
            _setterGame = setterGame;
            _isHelpModeEnabled = commandPlayParser.HasHelpParameter;
            _rowPosition = commandPlayParser.RowPosition;
            _columnPosition = commandPlayParser.ColumnPosition;
            _mark = commandPlayParser.MarkParameter;
            _positionString = commandPlayParser.PositionString;
        }
        #endregion

        #region Methods
        public static CommandPlay Create(IGame gamePlayer, ISetterGame setterGame, List<string> parameters)
        {
            CommandPlayParser commandPlayParser = new CommandPlayParser(setterGame, parameters);
            if (commandPlayParser.HasCorrectParameters())
            {
                return new CommandPlay(gamePlayer, setterGame, commandPlayParser);
            }
            else
            {
                throw new ArgumentException("Error: Unrecognized command parameter.");
            }
        }

        public void PrintHelpMessage()
        {
            string helpMessage = CommandConfigurationReader.GetHelpMessageFromConfigurationInfo("commandPlay");
            Console.WriteLine(helpMessage);
        }

        public void Execute()
        {
            if (_isHelpModeEnabled)
            {
                PrintHelpMessage();
            }
            else if (_setterGame.IsGameStarted)
            {
                Player player = GetPlayerByMark(_mark);
                Point position = new Point(_columnPosition, _rowPosition);
                if (_setterGame.IsOnlineModeEnabled)
                {
                    ManageMoveOnOnlineMode(position);
                }
                else
                {
                    _gamePlayer.MakeAMove(player, position);
                }
                _gamePlayer.AnalyzeGame(player);
            }
            else
            {
                throw new ArgumentException("Error: Game has not started or is finished. Start a new game to play. ");
            }
        }

        public void ManageMoveOnOnlineMode(Point position)
        {
            if (_gamePlayer.IsMyTurn)
            {
                _gamePlayer.MakeALocalMoveOnOnlineMode(_gamePlayer.PlayerA, position, _positionString);
            }
            else
            {
                _gamePlayer.MakeAOpponentMoveOnOnlineMode(_gamePlayer.PlayerB, position);
            }
        }

        private Player GetPlayerByMark(Mark mark)
        {
            switch (mark)
            {
                case Mark.X:
                    {
                        return _gamePlayer.PlayerA;
                    }
                default:
                    {
                        return _gamePlayer.PlayerB;
                    }
            }
        }
        #endregion
    }
}
