﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeDEV.GameTypes
{
    enum Command { Start, Play, Board, Score, Help, Exit, Undefined };
}
