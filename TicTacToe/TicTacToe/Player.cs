﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.GameTypes;

namespace TicTacToe
{    internal class Player
    {
        #region Attributes
        private int _earnedPoints;
        private int _tiedPoints;
        private Mark _mark;
        private string _username;
        #endregion

        #region Properties
        public Mark Mark
        {
            get { return _mark; }
        }

        public int EarnedPoints
        {
            get { return _earnedPoints; }
        }

        public int TiedPoints
        {
            get { return _tiedPoints; }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }
        #endregion

        #region Constructors
        public Player(Mark teamPlayer)
        {
            _mark = teamPlayer;
        }
        #endregion

        #region Methods
        public void AddWinPoint()
        {
            _earnedPoints++;
        }

        public void AddTiePoint()
        {
            _tiedPoints++;
        }
        #endregion
    }
}
