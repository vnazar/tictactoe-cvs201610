﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeDEV
{
    internal class GameInput
    {
        #region Methods
        private static string[] _cleanedInput;

        public static string[] CommandReader()
        {
            Console.Write("TicTacToe> ");
            string inputText = Console.ReadLine();
            _cleanedInput = InputSpliter(inputText);
            InputToUpperCase();
            return _cleanedInput;
        }

        private static string[] InputSpliter(string inputText)
        {
            char splitPattern = ' ';
            return inputText.Split(splitPattern);
        }

        private static void InputToUpperCase()
        {
            _cleanedInput = _cleanedInput.Select(s => s.ToUpperInvariant()).ToArray();
        } 
        #endregion
    }
}
