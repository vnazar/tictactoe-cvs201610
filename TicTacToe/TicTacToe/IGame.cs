﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.GameTypes;

namespace TicTacToe
{
    internal interface IGame
    {
        #region Properties
        bool IsMyTurn { get; }
        Player PlayerA { get; }
        Player PlayerB { get; }
        #endregion

        #region Methods
        void MakeAMove(Player player, Point position);
        void AnalyzeGame(Player player);
        void MakeALocalMoveOnOnlineMode(Player player, Point position, string positionString);
        void MakeAOpponentMoveOnOnlineMode(Player player, Point position);
        #endregion
    }
}
