﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class GameManager
    {
        #region Attributes
        private int _gamesPlayedOffline;
        private int _gamesPlayedOnline;
        private Player _localPlayerA;
        private Player _localPlayerB;
        private Player _localPlayerOnlineMode;
        private List<Player> _onlineOpponents;
        #endregion

        #region Properties
        public Player LocalPlayerOnline
        {
            get { return _localPlayerOnlineMode; }
        }

        public Player LocalPlayerA
        {
            get { return _localPlayerA; }
        }

        public Player LocalPlayerB
        {
            get { return _localPlayerB; }
        }
        #endregion

        #region Constructors
        public GameManager()
        {
            _localPlayerOnlineMode = new Player(GameTypes.Mark.X);
            _localPlayerA = new Player(GameTypes.Mark.X);
            _localPlayerB = new Player(GameTypes.Mark.O);
            _onlineOpponents = new List<Player>();
        }
        #endregion

        #region Methods
        public Player GetOpponentPlayerOnlineByUsername(string username)
        {
            bool existPlayer = _onlineOpponents.Exists(x => x.Username == username);
            if (!existPlayer)
            {
                CreateOpponent(username);
            }
            return _onlineOpponents.Find(x => x.Username == username);
        }

        private void CreateOpponent(string username)
        {
            Player newPlayer = new Player(GameTypes.Mark.O);
            newPlayer.Username = username;
            _onlineOpponents.Add(newPlayer);
        }

        public void AddGamePlayedOffline()
        {
            _gamesPlayedOffline++;
        }

        public void AddGamePlayedOnline()
        {
            _gamesPlayedOnline++;
        }

        public void PrintScoreDataOfflineMode()
        {
            int scorePlayerA = _localPlayerA.EarnedPoints;
            int scorePlayerB = _localPlayerA.EarnedPoints;
            int tieGames = _localPlayerA.TiedPoints;
            string scoreDataMessage = "Current Score:\n\t{0} Games played\n\t{1} Games won by {2}\n\t{3} Games won by {4}\n\t{5} Game tied";
            Console.WriteLine(scoreDataMessage,
                _gamesPlayedOffline.ToString(),
                scorePlayerA.ToString(),
                _localPlayerA.Mark.ToString(),
                scorePlayerB.ToString(),
                _localPlayerB.Mark.ToString(),
                tieGames.ToString()
                );
        }

        public void PrintScoreDataOnlineMode()
        {
            Console.WriteLine("Current Score:\n\t{0} Games played online\n\t{1} Games won by you", _gamesPlayedOnline, _localPlayerOnlineMode.EarnedPoints);
            foreach (Player player in _onlineOpponents)
            {
                if(player.EarnedPoints > 0)
                {
                    Console.WriteLine("\n\t{0} Games won by {1}", player.EarnedPoints.ToString(), player.Username);
                }
                if(player.TiedPoints > 0)
                {
                    Console.WriteLine("\n\t{0} Game tied with {1}", player.TiedPoints.ToString(), player.Username);
                }
            }
        }
        #endregion
    }
}
