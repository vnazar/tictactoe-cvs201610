﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.GameTypes;

namespace TicTacToe
{
    internal class Board
    {
        #region Attributes
        private Mark[,] _matrix;
        private int _length;
        #endregion

        #region Properties
        public int Length
        {
            get { return _length; }
        }
        #endregion

        #region Constructors
        public Board(int length)
        {
            _matrix = new Mark[length, length];
            _length = length;
        }
        #endregion

        #region Methods
        public Mark GetCellMark(Point position)
        {
            return _matrix[position.PositionX, position.PositionY];
        }

        public void SetCellMark(Player player, Point position)
        {
            VerifyIfCellIsMarked(position);
            _matrix[position.PositionX, position.PositionY] = player.Mark;
        }

        private void VerifyIfCellIsMarked(Point position)
        {
            if(_matrix[position.PositionX, position.PositionY] != Mark.Empty)
            {
                throw new ArgumentException("Error: position is taken.");
            }
        }
        #endregion
    }
}
