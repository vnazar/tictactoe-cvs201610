﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeDEV.Commands.CommandsParametersParser
{
    internal class CommandStartParser
    {
        #region Constants
        private const int NUMBER_OF_MINIMUM_OF_PARAMETERS = 1;
        private const int MINIMUM_BOARD_SIZE_PERMITED = 1;
        private const int MAXIMUM_BOARD_SIZE_PERMITED = 9; 
        #endregion

        #region Attributes
        private List<string> _parameters;
        private int _numberOfParametersInList;
        private int _numberOfParametersCount;
        private int _sizeOfBoard;
        private bool _hasConsecutiveParameter;
        private bool _hasSilentParameter;
        private bool _hasHelpParameter;
        #endregion

        #region Properties
        public bool HasConsecutiveParameter
        {
            get { return _hasConsecutiveParameter; }
        }

        public bool HasSilentParameter
        {
            get { return _hasSilentParameter; }
        }

        public bool HasHelpParameter
        {
            get { return _hasHelpParameter; }
        }

        public int SizeOfBoard
        {
            get { return _sizeOfBoard; }
        }
        #endregion

        #region Constructor
        public CommandStartParser(List<string> parameters)
        {
            _parameters = parameters;
            _numberOfParametersInList = _parameters.Count;
        }
        #endregion

        #region Methods
        public bool HasCorrectParameters()
        {
            if (IsHelpParameterInParameters())
            {
                return true;
            }
            else
            {
                return HasMinimumOfParameters();
            }
        }

        private bool IsHelpParameterInParameters()
        {
            _hasHelpParameter = _parameters.Exists(x => x == "-H");
            return _hasHelpParameter;
        }

        private bool HasMinimumOfParameters()
        {
            bool hasMinimumOfParameters = _numberOfParametersInList >= NUMBER_OF_MINIMUM_OF_PARAMETERS;
            if (hasMinimumOfParameters)
            {
                ExecuteFindParametersMethods();
                bool isNumberOfParametersCountedEqualToNumberOfParametersInList = _numberOfParametersCount == _numberOfParametersInList;
                return isNumberOfParametersCountedEqualToNumberOfParametersInList;
            }
            else
            {
                return false;
            }
        }

        private void ExecuteFindParametersMethods()
        {
            FindSizeOfBoardParameter();
            FindConsecutiveParameter();
            FindSilentParameter();
        }

        // FindSizeOfBoardParameter: if the parameter is null o not is a numeric parameter, set _sizeOfBoard to 0.
        private void FindSizeOfBoardParameter()
        {
            int sizeOfBoard;

            // Find the first parameter, because that is the size of _board parameter.
            string sizeOfBoardParameter = _parameters.Last();
            if (!string.IsNullOrEmpty(sizeOfBoardParameter))
            {
                bool isNumeric = int.TryParse(sizeOfBoardParameter, out sizeOfBoard);
                if (isNumeric)
                {
                    _sizeOfBoard = sizeOfBoard;
                    IsSizeOfBoardInAValidRange(_sizeOfBoard);               
                    _numberOfParametersCount++;
                }
            }
        }

        private static bool IsSizeOfBoardInAValidRange(int sizeOfBoard)
        {
            bool isSizeOfBoardInAValidRange = MINIMUM_BOARD_SIZE_PERMITED <= sizeOfBoard && sizeOfBoard <= MAXIMUM_BOARD_SIZE_PERMITED;
            if (isSizeOfBoardInAValidRange)
            {
                return true;
            }
            else
            {
                throw new ArgumentException(string.Format("Error: Board size should be between {0} and {1}.", MINIMUM_BOARD_SIZE_PERMITED, MAXIMUM_BOARD_SIZE_PERMITED));
            }
        }

        private void FindConsecutiveParameter()
        {
            _hasConsecutiveParameter = _parameters.Exists(x => x == "--CONSECUTIVE");
            if (_hasConsecutiveParameter)
            {
                _numberOfParametersCount++;
            }
        }

        private void FindSilentParameter()
        {
            _hasSilentParameter = _parameters.Exists(x => x == "--SILENT");
            if (_hasSilentParameter)
            {
                _numberOfParametersCount++;
            }
        }
        #endregion
    }
}
