﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    internal interface ISetterGame
    {
        #region Properties
        bool IsGameStarted { get; }

        int MaxLengthOfBoard { get; }

        bool IsOnlineModeEnabled { get; }

        bool OpponentFound { get; }
        #endregion

        #region Methods
        void SetupGame(bool isConsecutiveModeEnabled, bool isSilentModeEnabled, bool isOnlineModeEnabled);
        void ExitGame();
        void CreateBoard(int sizeOfBoard);

        void StartOnlineGame();
        void SubscribeEventOpponentFound();
        void WaitForAOpponent();
        void SetupPlayers();
        #endregion
    }
}
