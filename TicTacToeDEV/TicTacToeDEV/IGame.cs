﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToeDEV.GameTypes;

namespace TicTacToeDEV
{
    internal interface IGame
    {
        #region Methods
        void PlayGame(Mark mark, Point position);
        void AnalyzeGame(Mark mark); 
        #endregion
    }
}
