﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeDEV.Commands
{
    internal interface ICommandInput
    {
        #region Methods
        void Execute();
        void PrintHelpMessage(); 
        #endregion
    }
}
