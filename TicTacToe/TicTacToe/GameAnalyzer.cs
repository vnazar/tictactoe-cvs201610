﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.GameTypes;

namespace TicTacToe
{
    internal class GameAnalyzer
    {
        #region Attributes
        private Board _board;
        private Player _player;
        private bool _hasAWinner;
        #endregion

        #region Properties
        public bool HasAWinner
        {
            get { return _hasAWinner; }
        }
        #endregion

        #region Constructors
        public GameAnalyzer(Board board)
        {
            _board = board;
        }
        #endregion

        #region Methods
        public bool IsGameOver(Player player)
        {
            _player = player;
            return IsAWinner() || IsATie();
            _hasAWinner = IsAWinner();
        }


        private bool IsAWinner()
        {
            // The variable 'index' can be the column in case of 'HasCompleteColumn' or the row in case of 'HasCompleteRow' methods.
            for (int index = 0; index < _board.Length; index++)
            {
                if (DoesBoardHasACompleteColumn(_player, index) || DoesBoardHasACompleteRow(_player, index))
                {
                    return true;
                }
            }
            return DoesBoardHasCompleteBottomLeftToTopRightDiagonal(_player) || DoesBoardHasCompleteTopLeftToBottomRightDiagonal(_player);
        }

        private bool DoesBoardHasACompleteColumn(Player player, int column)
        {
            bool isWinner = true;
            for (int row = 0; row < _board.Length; row++)
            {
                Point actualPosition = new Point(row, column);
                if (_board.GetCellMark(actualPosition) != player.Mark)
                {
                    isWinner = false;
                }
            }
            return isWinner;
        }

        private bool DoesBoardHasACompleteRow(Player player, int row)
        {
            bool isWinner = true;
            for (int column = 0; column < _board.Length; column++)
            {
                Point actualPosition = new Point(row, column);
                if (_board.GetCellMark(actualPosition) != player.Mark)
                {
                    isWinner = false;
                }
            }
            return isWinner;
        }

        private bool DoesBoardHasCompleteTopLeftToBottomRightDiagonal(Player player)
        {
            bool isWinner = true;
            for (int diagonalIndex = 0; diagonalIndex < _board.Length; diagonalIndex++)
            {
                Point actualPosition = new Point(diagonalIndex, diagonalIndex);
                if (_board.GetCellMark(actualPosition) != player.Mark)
                {
                    isWinner = false;
                }
            }
            return isWinner;
        }

        private bool DoesBoardHasCompleteBottomLeftToTopRightDiagonal(Player player)
        {
            int maxBoardIndex = _board.Length - 1;
            bool isWinner = true;
            for (int column = 0; column < _board.Length; column++)
            {
                int row = maxBoardIndex - column;
                Point actualPosition = new Point(row, column);
                if (_board.GetCellMark(actualPosition) != player.Mark)
                {
                    isWinner = false;
                }
            }
            return isWinner;
        }

        private bool IsATie()
        {
            for (int row = 0; row < _board.Length; row++)
            {
                if (IsRowEmpty(row))
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsRowEmpty(int row)
        {
            for (int column = 0; column < _board.Length; column++)
            {
                Point actualPosition = new Point(row, column);
                if (_board.GetCellMark(actualPosition) == Mark.Empty)
                {
                    return true;
                }
            }
            return false;
        } 
        #endregion
    }
}