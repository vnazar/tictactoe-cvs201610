﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToeDEV.Commands.CommandsParametersParser;
using TicTacToeDEV.Commands.CommandsConfigurationInfo;

namespace TicTacToeDEV.Commands
{
    internal class CommandHelp : ICommandInput
    {
        #region Attributes
        private bool _isHelpModeEnabled;
        #endregion

        #region Constructors
        public CommandHelp(bool hasHelpParameter)
        {
            // Setting command.
            _isHelpModeEnabled = hasHelpParameter;
        }
        #endregion

        #region Methods
        public static CommandHelp Create(List<string> parameters)
        {
            CommandOnlyHelpParameterParser commandHelpParser = new CommandOnlyHelpParameterParser(parameters);
            if (commandHelpParser.HasCorrectParameters())
            {
                return new CommandHelp(commandHelpParser.HasHelpParameter);
            }
            else
            {
                throw new ArgumentException("Error: Unrecognized command parameter.");
            }
        }

        public void PrintHelpMessage()
        {
            string helpMessage = CommandConfigurationReader.GetHelpMessageFromConfigurationInfo("commandHelp");
            Console.WriteLine(helpMessage);
        }

        public void Execute()
        {
            {
                if (_isHelpModeEnabled)
                {
                    PrintHelpMessage();
                }
                else
                {
                    PrintAllCommandsHelpMessage();
                }
            }
        }

        private static void PrintAllCommandsHelpMessage()
        {
            List<string> helpMessages = CommandConfigurationReader.GetAllHelpMessagesFromConfigurationInfo();
            foreach (var helpMessage in helpMessages)
            {
                Console.WriteLine(helpMessage + "\n");
            }
        }
        #endregion
    }
}

