﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.GameTypes;
using TicTacToe;

namespace TicTacToe.Commands.CommandsParametersParser
{
    internal class CommandPlayParser
    {
        #region Constants
        private const int NUMBER_OF_MINIMUM_OF_PARAMETERS = 1;
        #endregion

        #region Attributes
        private ISetterGame _setterGame;
        private List<string> _parameters;
        private string _positionString;
        private int _numberOfParametersInList;
        private int _columnPosition;
        private int _rowPosition;
        private Mark _markParameter;
        private int _maxLengthOfBoard;
        private bool _hasHelpParameter;
        private string _alphabet;
        private bool _isOnlineModeEnabled;
        private int _indexOfMark;
        private int _indexOfPosition;
        #endregion

        #region Properties
        public Mark MarkParameter
        {
            get { return _markParameter; }
        }

        public int ColumnPosition
        {
            get { return _columnPosition; }
        }

        public int RowPosition
        {
            get { return _rowPosition; }
        }

        public bool HasHelpParameter
        {
            get { return _hasHelpParameter; }
        }

        public string PositionString
        {
            get { return _positionString; }
        }
        #endregion

        #region Constructors
        public CommandPlayParser(ISetterGame setterGame, List<string> parameters)
        {
            _setterGame = setterGame;
            _parameters = parameters;
            _numberOfParametersInList = _parameters.Count;
            _maxLengthOfBoard = setterGame.MaxLengthOfBoard;
            _alphabet = Game.ALPHABET;
            _isOnlineModeEnabled = setterGame.IsOnlineModeEnabled;
            _indexOfMark = 0;
            _indexOfPosition = 1;
        }
        #endregion

        #region Methods
        public bool HasCorrectParameters()
        {
            if (IsHelpParameterInParameters())
            {
                return true;
            }
            else
            {
                return HasMinimumOfParameters();
            }
        }

        private bool HasMinimumOfParameters()
        {
            bool hasMinimumOfParameters = _numberOfParametersInList >= NUMBER_OF_MINIMUM_OF_PARAMETERS;
            if (hasMinimumOfParameters)
            {
                ExecuteFindParametersMethods();
                bool isAllParametersWereFound = _parameters.Count == 0;
                return isAllParametersWereFound;
            }
            else
            {
                return false;
            }
        }

        private bool IsHelpParameterInParameters()
        {
            _hasHelpParameter = _parameters.Exists(x => x == "-H");
            return _hasHelpParameter;
        }

        private void ExecuteFindParametersMethods()
        {
            if (_isOnlineModeEnabled)
            {
                _indexOfPosition = 0;
                FindPositionParameter();
            }
            else
            {
                FindPositionParameter();
                FindMarkParameter();   
            }
            
        }

        private void FindMarkParameter()
        {
            string markParameter = _parameters[_indexOfMark];
            try
            {
                _markParameter = (Mark)Enum.Parse(typeof(Mark), markParameter);
                _parameters.RemoveAt(_indexOfMark);
            }
            catch
            {
                throw new ArgumentException("Error: Mark parameter must be a O or X.");
            }
        }

        private void FindPositionParameter()
        {
            string positionParameter = _parameters[_indexOfPosition];
            int standardLengthOfPositionParameter = 2;
            bool IsCorrectThePositionParameterLength = positionParameter.Length == standardLengthOfPositionParameter;
            if (IsCorrectThePositionParameterLength)
            {
                FindIfRowPositionIsValid(positionParameter);
                FindIfColumnPositionIsValid(positionParameter);
            }
        }

        private void FindIfRowPositionIsValid(string positionParameter)
        {
            int indexOfRowParameterInPositionParameter = 0;
            char rowPosition = positionParameter[indexOfRowParameterInPositionParameter];
            if (Game.ALPHABET.Contains(rowPosition))
            {
                int indexOfRowPositionInAlphabet = _alphabet.IndexOf(rowPosition);
                VerifyIfRowPositionIsInRange(indexOfRowPositionInAlphabet);
            }
            else
            {
                throw new ArgumentException("Error: Row position must be and alphabet letter.");
            }
        }

        private void VerifyIfRowPositionIsInRange(int indexOfRowPositionInAlphabet)
        {
            bool isRowPositionInRange = 0 <= indexOfRowPositionInAlphabet && indexOfRowPositionInAlphabet < _maxLengthOfBoard;
            if (isRowPositionInRange)
            {
                _rowPosition = indexOfRowPositionInAlphabet;
            }
            else
            {
                throw new ArgumentException(string.Format("Error: Row position should be between A and {0} in the alphabet.", _alphabet[_maxLengthOfBoard - 1].ToString()));
            }
        }

        private void FindIfColumnPositionIsValid(string positionParameter)
        {
            int indexOfColumnParameterInPositionParameter = 1;
            char columPositionFromPositionParameter = positionParameter[indexOfColumnParameterInPositionParameter];
            int columnPosition = (int)char.GetNumericValue(columPositionFromPositionParameter);
            bool isColumnPositionInColumnRange = 0 < columnPosition && columnPosition <= _maxLengthOfBoard;
            if (isColumnPositionInColumnRange)
            {
                _columnPosition = columnPosition - 1;
                _positionString = _parameters[_indexOfPosition];
                _parameters.RemoveAt(_indexOfPosition);
            }
            else
            {
                throw new ArgumentException(string.Format("Error: Column position should be between 1 and {0}.", _maxLengthOfBoard.ToString()));
            }
        }
        #endregion
    }
}
