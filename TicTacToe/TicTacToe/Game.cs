﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TicTacToe.Commands;
using TicTacToe.GameTypes;
using System.Threading;
using ExternalComponent;
using TicTacToe.Commands.CommandsParametersParser;

namespace TicTacToe
{
    internal class Game : IGame, ISetterGame, IPrinterGame
    {
        #region Constants
        public const string ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        #endregion

        #region Attributes
        // Flags of game.
        private bool _isExitOptionEnabled;
        private bool _isConsecutiveModeEnabled;
        private bool _isSilentModeEnabled;
        private bool _isOnlineModeEnabled;

        // Online mode.
        private IExternalComponent _externalComponent;
        private bool _opponentFound;
        private bool _isMyTurn;
        private string _opponentName;

        private Board _board;
        private bool _isGameStarted;
        private Player _playerA;
        private Player _playerB;
        private int _maxLengthOfBoard;
        private GameAnalyzer _gameAnalyzer;
        private BoardPrinter _boardPrinter;
        private Player _lastPlayerToMakeAMove;
        private GameManager _gameManager;
        private Point _lastMovePlayerPosition;
        #endregion

        #region Properties
        public bool IsGameStarted
        {
            get { return _isGameStarted; }
        }

        public int MaxLengthOfBoard
        {
            get { return _maxLengthOfBoard; }
        }

        public bool IsOnlineModeEnabled
        {
            get { return _isOnlineModeEnabled; }
        }

        public bool OpponentFound
        {
            get { return _opponentFound; }
        }

        public bool IsMyTurn
        {
            get { return _isMyTurn; }
        }

        public Player PlayerA
        {
            get { return _playerA; }
        }

        public Player PlayerB
        {
            get { return _playerB; }
        }
        #endregion

        #region Constructors
        public Game()
        {
            // Setting console.
            Console.Title = "TicTacToe";

            // Setup the game.
            _isExitOptionEnabled = false;
            _isGameStarted = false;
            _gameManager = new GameManager();
            _externalComponent = new ExternalComponentAdapter();
        }
        #endregion

        #region Methods
        public void ExecuteGameLoop()
        {
            Console.WriteLine("Hello! How about a game of TicTacToe?");
            while (_isExitOptionEnabled == false)
            {
                string[] commandInput = GameInput.CommandReader();
                try
                {
                    var createdCommand = CommandFactory.CreateCommand(this, commandInput);
                    createdCommand.Execute();
                }
                catch (ArgumentException exception)
                {
                    Console.WriteLine(exception.Message);
                }
            }
        }

        public void SetupGame(bool isConsecutiveModeEnabled, bool isSilentModeEnabled, bool isOnlineModeEnabled)
        {
            _isConsecutiveModeEnabled = isConsecutiveModeEnabled;
            _isSilentModeEnabled = isSilentModeEnabled;
            _isOnlineModeEnabled = isOnlineModeEnabled;
            _isGameStarted = true;
            _boardPrinter = new BoardPrinter(_board);
            _gameAnalyzer = new GameAnalyzer(_board);
        }

        public void SetupPlayers()
        {
            if (_isOnlineModeEnabled)
            {
                _playerA = _gameManager.LocalPlayerOnline;
                _playerB = _gameManager.GetOpponentPlayerOnlineByUsername(_opponentName);
            }
            else
            {
                _playerA = _gameManager.LocalPlayerA;
                _playerB = _gameManager.LocalPlayerB;
            }
        }

        public void ExitGame()
        {
            int millisecondsForTimer = 5000;
            Console.WriteLine("Bye!");
            Thread.Sleep(millisecondsForTimer);
            _isExitOptionEnabled = true;
        }

        public void CreateBoard(int sizeOfBoard)
        {
            _board = new Board(sizeOfBoard);
            _maxLengthOfBoard = sizeOfBoard;
        }

        public void PrintBoardTable()
        {
            _boardPrinter.LastMovePlayerPosition = _lastMovePlayerPosition;
            _boardPrinter.PrintBoard();
        }

        public void MakeAMove(Player player, Point position)
        {
            if (!_isConsecutiveModeEnabled && _lastPlayerToMakeAMove == player)
            {
                throw new ArgumentException("Error: It is the turn of the next player.");
            }
            _board.SetCellMark(player, position);
            _lastMovePlayerPosition = position;
            _lastPlayerToMakeAMove = player;
            if (!_isSilentModeEnabled)
            {
                PrintBoardTable();
            }
        }

        public void AnalyzeGame(Player player)
        {
            if (_gameAnalyzer.IsGameOver(player))
            {
                AddGamePlayed();
                if (_gameAnalyzer.HasAWinner)
                {
                    player.AddWinPoint();
                    PrintEndGameMessageForLocalPlayer(player);
                }
                else
                {
                    _playerA.AddTiePoint();
                    _playerB.AddTiePoint();
                    Console.WriteLine("Is a Tie.");
                }
                PrintScoreData();
                ResetGame();
            }
        }

        public void PrintScoreData()
        {
            if (_isOnlineModeEnabled)
            {
                _gameManager.PrintScoreDataOnlineMode();
            }
            else
            {
                _gameManager.PrintScoreDataOfflineMode();
            } 
        }

        public void AddGamePlayed()
        {
            if (_isOnlineModeEnabled)
            {
                _gameManager.AddGamePlayedOnline();
            }
            else
            {
                _gameManager.AddGamePlayedOffline();
            }
        }

        public void PrintEndGameMessageForLocalPlayer(Player player)
        {
            if (_isOnlineModeEnabled)
            {
                if(player == _playerA)
                {
                    Console.WriteLine("You won!");
                }
                else
                {
                    Console.WriteLine("You lose!");
                }
            }
            else
            {
                Console.WriteLine("Player {0} won", player.Mark.ToString());
            }
        }

        private void ResetGame()
        {
            _isConsecutiveModeEnabled = false;
            _isSilentModeEnabled = false;
            _isGameStarted = false;
            _opponentFound = false;
            if (_isOnlineModeEnabled)
            {
                _externalComponent.OpponentMoved -= OnOpponentMoved;
                _externalComponent.CloseConnection();
            }
            _isOnlineModeEnabled = false;
        }

        public void StartOnlineGame()
        {
            try
            {
                _externalComponent.InitializeConnection();
                _externalComponent.RequestOnlineOpponent(_maxLengthOfBoard);
            }
            catch (System.Net.WebException)
            {
                throw new ArgumentException("Error: Internet connection suddenly interrupted!");
            }
        }

        public void SubscribeEventOpponentFound()
        {
            _externalComponent.OpponentFound += OnOpponentFound;
        }

        public void OnOpponentFound(string name, bool localMovesFirst)
        {
            _opponentFound = true;
            _isMyTurn = localMovesFirst;
            _opponentName = name;
            _externalComponent.OpponentMoved += OnOpponentMoved;
            WaitForAOpponentMove();
        }

        public void WaitForAOpponent()
        {
            Console.WriteLine("Waiting for the opponent... ");
            Run30SecondsTimer(_opponentFound);
            if (!_opponentFound)
            {
                Console.WriteLine("Error: The opponent has not been found.");
                ResetGame();
            }
            _externalComponent.OpponentFound -= OnOpponentFound;
        }

        public static void Run30SecondsTimer(bool conditionToStop)
        {
            for (int seconds = 30; seconds >= 0; seconds--)
            {
                if (conditionToStop)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
        }

        public void OnOpponentMoved(string move)
        {
            _isMyTurn = true;
            string[] commandInput = new string[] { "PLAY", move };
            var createdCommand = CommandFactory.CreateCommand(this, commandInput);
            createdCommand.Execute();
        }

        public void WaitForAOpponentMove()
        {
            if (!_isMyTurn)
            {
                Console.WriteLine("Waiting for {0} move... ", _opponentName);
                Run30SecondsTimer(_isMyTurn);
                if (!_isMyTurn)
                {
                    Console.WriteLine("Error: Time out. It seems {0} has left the game.", _opponentName);
                    ResetGame();
                }
            }
        }

        public void MakeALocalMoveOnOnlineMode(Player player, Point position, string positionString)
        {
            _board.SetCellMark(player, position);
            _externalComponent.SetMyLastMove(positionString);
            if (!_isSilentModeEnabled)
            {
                _lastMovePlayerPosition = position;
                PrintBoardTable();
            }
            _isMyTurn = false;
            WaitForAOpponentMove();

        }

        public void MakeAOpponentMoveOnOnlineMode(Player player, Point position)
        {
            _board.SetCellMark(player, position);
            _lastMovePlayerPosition = position;
            PrintBoardTable();
        }
        #endregion
    }
}