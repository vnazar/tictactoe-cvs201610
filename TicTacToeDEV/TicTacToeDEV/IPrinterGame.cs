﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeDEV
{
    internal interface IPrinterGame
    {
        #region Methods
        void PrintScoreData();
        void PrintBoardTable(); 
        #endregion
    }
}
