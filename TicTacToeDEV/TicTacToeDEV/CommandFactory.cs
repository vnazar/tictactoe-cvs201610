﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TicTacToeDEV.Commands;
using TicTacToeDEV.GameTypes;

namespace TicTacToeDEV
{
    static class CommandFactory
    {
        #region Methods
        public static ICommandInput CreateCommand(Game currentGame, string[] gameInput)
        {
            
            int indexOfCommandInCommandInput = 0;
            string commandFieldFromGameInput = gameInput[indexOfCommandInCommandInput];
            Command command = ParseCommand(commandFieldFromGameInput);
            List<string> parameters = GetListOfCommandParameters(gameInput);
            switch (command)
            {
                case Command.Start:
                    {
                        ICommandInput commandStart = CommandStart.Create(currentGame, parameters);
                        return commandStart;
                    }
                case Command.Play:
                    {
                        ICommandInput commandPlay = CommandPlay.Create(currentGame, currentGame, parameters);
                        return commandPlay;
                    }
                case Command.Board:
                    {
                        ICommandInput commandBoard = CommandBoard.Create(currentGame, parameters);
                        return commandBoard;
                    }
                case Command.Score:
                    {
                        ICommandInput commandScore = CommandScore.Create(currentGame, parameters);
                        return commandScore;
                    }
                case Command.Help:
                    {
                        ICommandInput commandHelp = CommandHelp.Create(parameters);
                        return commandHelp;
                    }
                case Command.Exit:
                    {
                        ICommandInput commandExit = CommandExit.Create(currentGame, parameters);
                        return commandExit;
                    }
                default:
                    throw new ArgumentException("Error: Unrecognized command");
            }
        }

        public static Command ParseCommand(string command)
        {
            try
            {
                return (Command)Enum.Parse(typeof(Command), command, true);
            }
            catch(ArgumentException)
            {
                return Command.Undefined;
            }
        }

        private static List<string> GetListOfCommandParameters(string[] commandInput)
        {
            int commandIndex = 1;
            List<string> commandParameters = new List<string>();
            foreach (string parameter in commandInput.Skip(commandIndex))
            {
                commandParameters.Add(parameter);
            }
            return commandParameters;
        } 
        #endregion
    }
}
