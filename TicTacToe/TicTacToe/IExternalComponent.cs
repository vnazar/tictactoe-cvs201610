﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public interface IExternalComponent
    {
        void InitializeConnection();

        void RequestOnlineOpponent(int sizeOfBoard);

        event Action<string, bool> OpponentFound;

        string GetOpponentName();

        bool LocalPlayerMovesFirst();

        event Action<string> OpponentMoved;

        string GetOpponentLastMove();

        void SetMyLastMove(string position);

        void CloseConnection();
    }
}
