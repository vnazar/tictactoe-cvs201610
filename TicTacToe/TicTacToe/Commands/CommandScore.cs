﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Commands.CommandsParametersParser;
using TicTacToe.Commands.CommandsConfigurationInfo;

namespace TicTacToe.Commands
{
    internal class CommandScore : ICommandInput
    {
        #region Attributes
        private IPrinterGame _printerGame;
        private bool _isHelpModeEnabled;
        #endregion

        #region Constructor
        public CommandScore(IPrinterGame printerGame, bool hasHelpParameter)
        {
            // Setting command.
            _printerGame = printerGame;
            _isHelpModeEnabled = hasHelpParameter;
        }
        #endregion

        #region Methods
        public static CommandScore Create(IPrinterGame printerGame, List<string> parameters)
        {
            CommandOnlyHelpParameterParser commandScoreParser = new CommandOnlyHelpParameterParser(parameters);
            if (commandScoreParser.HasCorrectParameters())
            {
                return new CommandScore(printerGame, commandScoreParser.HasHelpParameter);
            }
            else
            {
                throw new ArgumentException("Error: Unrecognized command parameter.");
            }
        }

        public void PrintHelpMessage()
        {
            string helpMeassage = CommandConfigurationReader.GetHelpMessageFromConfigurationInfo("commandScore");
            Console.WriteLine(helpMeassage);
        }

        public void Execute()
        {
            if (_isHelpModeEnabled)
            {
                PrintHelpMessage();
            }
            else
            {                 
                _printerGame.PrintScoreData();
            }
        }
        #endregion
    }
}