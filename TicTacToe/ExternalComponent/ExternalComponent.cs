﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalComponent
{
    public class ExternalComponent
    {
        public static event Action<string> OpponentMoved;
        public static event Action<string, bool> OpponentFound;

        public static void InitializeConnection()
        {
            Random rnd = new Random();
            if (rnd.Next(1,5) == 3)
            {
                throw new System.Net.WebException("No connection");
            }
        }

        public static void RequestOnlineOpponent(int sizeOfBoard)
        { }

        public static string GetOpponentName()
        {
            return "vicente";
        }

        public static bool LocalPlayerMovesFirst()
        {
            return true;
        }

        public static string GetOpponentLastMove()
        {
            return "A1";
        }

        public static void SetMyLastMove(string position)
        { }

        public static void CloseConnection()
        { }
    }
}
