﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToeDEV.GameTypes;

namespace TicTacToeDEV
{    internal class Player
    {
        #region Attributes
        private int _earnedPoints;
        private Mark _teamPlayer;
        #endregion

        #region Properties
        public Mark TeamPlayer
        {
            get { return _teamPlayer; }
        }

        public int EarnedPoints
        {
            get { return _earnedPoints; }
        }
        #endregion

        #region Constructors
        public Player(Mark teamPlayer)
        {
            _teamPlayer = teamPlayer;
        }
        #endregion

        #region Methods
        public void AddWinPoint()
        {
            _earnedPoints++;
        } 
        #endregion
    }
}
