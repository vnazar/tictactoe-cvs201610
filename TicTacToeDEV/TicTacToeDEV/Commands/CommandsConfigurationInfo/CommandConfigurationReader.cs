﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace TicTacToeDEV.Commands.CommandsConfigurationInfo
{
    internal class CommandConfigurationReader
    {
        #region Attributes
        private static readonly string pathOfCommandsConfigurationFile = "..\\..\\Commands\\CommandsConfigurationInfo\\CommandsConfigurationInfo.xml"; 
        #endregion

        #region Methods
        public static string GetHelpMessageFromConfigurationInfo(string commandName)
        {
            var xDoc = XDocument.Load(pathOfCommandsConfigurationFile);
            var nodeOfSpecificCommand = xDoc.Element("configuration").Elements("command").Single(x => (string)x.Attribute("name") == commandName);
            string helpMessage = Regex.Unescape(nodeOfSpecificCommand.Element("helpMessage").Value);
            return helpMessage;
        }

        public static List<string> GetAllHelpMessagesFromConfigurationInfo()
        {
            List<string> listOfHelpMessages = new List<string>();
            var xDoc = XDocument.Load(pathOfCommandsConfigurationFile);
            foreach(var element in xDoc.Descendants("command"))
            {
                listOfHelpMessages.Add(Regex.Unescape(element.Value));
            }
            return listOfHelpMessages;
        }
        #endregion
    }
}
