﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeDEV.Commands
{
    interface ICommand
    {
        void Execute();
        void PrintHelpCommand();
    }
}
