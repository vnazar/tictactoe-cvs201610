﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToeDEV.Commands.CommandsConfigurationInfo;
using TicTacToeDEV.Commands.CommandsParametersParser;
using TicTacToeDEV.GameTypes;

namespace TicTacToeDEV.Commands
{
    internal class CommandPlay : ICommandInput
    {
        #region Attributes
        private IGame _playGame;
        private bool _isHelpModeEnabled;
        private int _rowPosition;
        private int _columnPosition;
        private Mark _mark;
        #endregion

        #region Constructor
        public CommandPlay(IGame playGame, int rowPosition, int columnPosition, Mark mark, bool hasHelpParameter)
        {
            // Setting command.
            _playGame = playGame;
            _isHelpModeEnabled = hasHelpParameter;
            _rowPosition = rowPosition;
            _columnPosition = columnPosition;
            _mark = mark;
        }
        #endregion

        #region Methods
        public static CommandPlay Create(IGame playGame, ISetterGame setterGame, List<string> parameters)
        {
            CommandPlayParser commandPlayParser = new CommandPlayParser(setterGame, parameters);
            if (commandPlayParser.HasCorrectParameters())
            {
                return new CommandPlay(playGame, commandPlayParser.RowPosition, commandPlayParser.ColumnPosition, commandPlayParser.MarkParameter, commandPlayParser.HasHelpParameter);
            }
            else
            {
                throw new ArgumentException("Error: Unrecognized command parameter.");
            }
        }

        public void PrintHelpMessage()
        {
            string helpMessage = CommandConfigurationReader.GetHelpMessageFromConfigurationInfo("commandPlay");
            Console.WriteLine(helpMessage);
        }

        public void Execute()
        {
            if (_isHelpModeEnabled)
            {
                PrintHelpMessage();
            }
            else
            {
                Point position = new Point(_columnPosition, _rowPosition);
                _playGame.PlayGame(_mark, position);
                _playGame.AnalyzeGame(_mark);
            }
        }
        #endregion
    }
}
