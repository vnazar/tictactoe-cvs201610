﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Commands.CommandsParametersParser;
using TicTacToe.Commands.CommandsConfigurationInfo;

namespace TicTacToe.Commands
{
    internal class CommandExit : ICommandInput
    {
        #region Attributes
        private ISetterGame _setterGame;
        private bool _isHelpModeEnabled;
        #endregion

        #region Constructors
        public CommandExit(ISetterGame setterGame, bool hasHelpParameter)
        {
            // Setting command.
            _setterGame = setterGame;
            _isHelpModeEnabled = hasHelpParameter;
        }
        #endregion

        #region Methods
        public static CommandExit Create(ISetterGame setterGame, List<string> parameters)
        {
            CommandOnlyHelpParameterParser commandScoreParser = new CommandOnlyHelpParameterParser(parameters);
            if (commandScoreParser.HasCorrectParameters())
            {
                return new CommandExit(setterGame, commandScoreParser.HasHelpParameter);
            }
            else
            {
                throw new ArgumentException("Error: Unrecognized command parameter.");
            }
        }

        public void PrintHelpMessage()
        {
            string helpMeassage = CommandConfigurationReader.GetHelpMessageFromConfigurationInfo("commandExit");
            Console.WriteLine(helpMeassage);
        }

        public void Execute()
        {
            {
                if (_isHelpModeEnabled)
                {
                    PrintHelpMessage();
                }
                else
                {
                    _setterGame.ExitGame();
                }
            }
        } 
        #endregion
    }
}
